/*
 * DicomDB: org.nrg.dcm.DicomAttributeIndex
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.nrg.attr.ConversionFailureException;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface DicomAttributeIndex {
	String getAttributeName(DicomObject o);

	/**
	 * Returns the name of the column for the target attribute in the SQL database.
	 * @return
	 */
	String getColumnName();
	
	/**
	 * Returns the tag path for this attribute index. Requires a context argument
	 * because private tags are relocatable. Note that this path is not quite equivalent
	 * to the dcm4che int[] tagpath, because we can use null values as a sequence index
	 * meaning use all of the sequence items.
	 * @return
	 */
	Integer[] getPath(DicomObject context);
	
	DicomElement getElement(DicomObject o);

	String getString(DicomObject o) throws ConversionFailureException;
	String getString(DicomObject o, String defaultValue) throws ConversionFailureException;
	
	String[] getStrings(DicomObject o);
	
	public static class Comparator implements java.util.Comparator<DicomAttributeIndex> {
		public int compare(final DicomAttributeIndex i0, final DicomAttributeIndex i1) {
			return i0.getColumnName().compareTo(i1.getColumnName());
		}
	}
}
