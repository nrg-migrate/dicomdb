/*
 * DicomDB: org.nrg.dcm.AttrAdapter
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm;

import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.nrg.attr.AbstractAttrAdapter;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.EvaluableAttrDef;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;

/**
 * For a given FileSet, generates external attributes from the corresponding
 * DICOM fields.
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class AttrAdapter extends AbstractAttrAdapter<DicomAttributeIndex,String> {
    private static final Map<DicomAttributeIndex,String> EMPTY_CONTEXT = Collections.emptyMap();

    private final Logger logger = LoggerFactory.getLogger(AttrAdapter.class);
    private final DicomMetadataStore store;
    private final Map<?,String> context;

    public AttrAdapter(final DicomMetadataStore store, final Map<?,String> context,
            final AttrDefs...attrs) {
        super(new MutableAttrDefs(), attrs);
        this.store = store;
        this.context = ImmutableMap.copyOf(context);
    }

    /**
     * Creates a new attribute adapter for the given FileSet
     * @param store DICOM FileSet
     * @param attrs AttributeSets for conversion
     */
    public AttrAdapter(final DicomMetadataStore fs, final AttrDefs...attrs) {
        this(fs, EMPTY_CONTEXT, attrs);
    }

    public Collection<Map<DicomAttributeIndex,String>>
    getUniqueCombinationsGivenValues(final Map<DicomAttributeIndex,String> given,
            final Collection<DicomAttributeIndex> attrs,
            final Map<DicomAttributeIndex,ConversionFailureException> failures){
        try {
            final Map<Object,String> g = Maps.newLinkedHashMap(context);
            g.putAll(given);
            return store.getUniqueCombinationsGivenValues(g, attrs, failures);
        } catch (Throwable t) {
            for (final DicomAttributeIndex attr : attrs) {
                failures.put(attr, new ConversionFailureException(attr, null, "conversion failed", t));
            }
            return Collections.emptyList();
       }
    }

    /**
     * For each file, returns the single value of each specified attribute
     * @return map from each file to a list of external attribute values
     * @throws IOException
     * @throws NoUniqueValueException
     * @throws ExtAttrConversionException
     */
    public ListMultimap<URI,ExtAttrValue> getValuesForResources()
            throws IOException,SQLException {
        final ListMultimap<URI,ExtAttrValue> values = ArrayListMultimap.create();  
        for (final Map.Entry<URI,Map<DicomAttributeIndex,String>> fme
                : store.getValuesForResourcesMatching(getDefs().getNativeAttrs(), context).entrySet()) {
            final Iterable<Map<DicomAttributeIndex,String>> vals = Collections.singletonList(fme.getValue());
            for (final ExtAttrDef<DicomAttributeIndex> ea : getDefs()) {
                try {
                    @SuppressWarnings("unchecked")
                    final EvaluableAttrDef<DicomAttributeIndex,String,?> def =
                            (EvaluableAttrDef<DicomAttributeIndex,String,?>)ea;
                    values.putAll(fme.getKey(), def.foldl(vals));
                } catch (ExtAttrException e) {
                    // TODO: export this failure
                    final StringBuilder sb = new StringBuilder("Unable to build attribute ");
                    sb.append(ea).append(" from file ").append(fme.getKey());
                    logger.warn(sb.toString(), e);
                }
            }
        }
        return values;
    }
}
