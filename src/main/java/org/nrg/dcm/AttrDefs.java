/*
 * DicomDB: org.nrg.dcm.AttrDefs
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/**
 * Copyright 2010 Washington University
 */
package org.nrg.dcm;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface AttrDefs extends org.nrg.attr.AttrDefs<DicomAttributeIndex> {}
