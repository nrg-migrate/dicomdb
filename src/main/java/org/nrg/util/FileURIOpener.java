/*
 * DicomDB: org.nrg.util.FileURIOpener
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Opens URIs that are local file references.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class FileURIOpener implements Opener<URI> {
    /**
     * Returns a relative (no scheme) URI for the provided (possibly absolute) File
     * @param f
     * @return
     */
    public static URI toURI(final File f) {
        try {
            return new URI(f.getPath());
        } catch (URISyntaxException e) {
            return f.toURI();
        }
    }
    
    private final Logger logger = LoggerFactory.getLogger(FileURIOpener.class);

    private static FileURIOpener instance = new FileURIOpener();
    
    public static FileURIOpener getInstance() { return instance; }

    private FileURIOpener() {}
    
    /*
     * (non-Javadoc)
     * @see org.nrg.util.IOUtils.Opener#open(java.lang.Object)
     */
    @SuppressWarnings("resource")
    public InputStream open(final URI uri) throws IOException {
        logger.debug("opening {} as {}", uri, uri.getPath());
        final String path = uri.getPath();
        final InputStream in = new FileInputStream(uri.getPath());
        return path.endsWith(".gz") ? new GZIPInputStream(in) : in;
    }
}
